#Author: José Santiago Molano Perdomo, jsmolano@bancolombia.com.co, jmolanop@choucairtesting.com
@Regresion
Feature: Cargar archivo de facturación bancolombia 2016 en el menú UBPF de Finacle
  @exitoso
  Scenario: cargar facturas
    Given that Santiago wants to generate the Bancolombia billing file to carry out its tests
    When  when he opens finacle to authenticate
    |username|password|
    |jsmolano|bcol@123|
    And he goes to ubpf menu
