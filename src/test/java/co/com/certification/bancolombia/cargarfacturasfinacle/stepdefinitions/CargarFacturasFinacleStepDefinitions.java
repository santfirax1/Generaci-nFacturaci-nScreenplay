package co.com.certification.bancolombia.cargarfacturasfinacle.stepdefinitions;

import co.com.certification.bancolombia.cargarfacturasfinacle.model.LogIn;
import co.com.certification.bancolombia.cargarfacturasfinacle.tasks.Authenticate;
import co.com.certification.bancolombia.cargarfacturasfinacle.tasks.GenerarArchivo;
import co.com.certification.bancolombia.cargarfacturasfinacle.tasks.GoTo;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.actors.OnStage;

import java.util.List;

public class CargarFacturasFinacleStepDefinitions {

    @Given("^that (.*) wants to generate the Bancolombia billing file to carry out its tests$")
    public void thatSantiagoWantsToGenerateTheBancolombiaBillingFileToCarryOutItsTests(String nameOfTheActor)  {
      OnStage.theActorCalled(nameOfTheActor).wasAbleTo(GenerarArchivo.bancolombia2016());
    }

    @When("^when he opens finacle to authenticate$")
    public void whenHeOpensFinacleToAuthenticate(List<LogIn> logInList)  {
     OnStage.theActorInTheSpotlight().attemptsTo(Authenticate.inFinacle(logInList));
     OnStage.theActorInTheSpotlight().attemptsTo(GoTo.coreFirecIntSolution());

    }

    @And("^he goes to ubpf menu$")
    public void heGoesToUbpfMenu(){

    }
}
