package co.com.certification.bancolombia.cargarfacturasfinacle.runner;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;
@CucumberOptions(
        features = "src/test/resources/features/cargar_facturas_bancolombia_en_finacle.feature",
        tags = "@exitoso",
        glue = {"co.com.certification.bancolombia.cargarfacturasfinacle.stepdefinitions","co.com.certification.bancolombia.cargarfacturasfinacle.util"}
)
@RunWith(CucumberWithSerenity.class)
public class CargarFacturasFinacleRunner {
}
