package co.com.certification.bancolombia.cargarfacturasfinacle.model;

public class FileControlRecord {
    private int lengthBillerEntityNit;
    private int lengthBillerServiceIdCode;
    private int lengthApplicationOfExpirationDate;
    private int lengthRecordsNumber;
    private int lengthTotalAmountofTheTransactions;
    private String recordType;
    private String billerEntityNit;
    private String nameOfBillerEntity;
    private String billerServiceIdCode;
    private String fileTransmisionDate;
    private String sendingSequence;
    private String applicationOrExpiration;
    private String recordsNumber;
    private String totalAmountOfTransactions;
    private String encabezadoTotalFileControlRecord;

    public void setEncabezadoTotalFileControlRecord(String encabezadoTotalFileControlRecord) {
        this.encabezadoTotalFileControlRecord = encabezadoTotalFileControlRecord;
    }

    public String getEncabezadoTotalFileControlRecord() {
        return encabezadoTotalFileControlRecord;
    }

    private String reserved;

    public FileControlRecord() {
        lengthBillerEntityNit = 13;
        lengthBillerServiceIdCode = 15;
        lengthApplicationOfExpirationDate = 8;
        lengthRecordsNumber = 8;
        lengthTotalAmountofTheTransactions = 17;
        recordType = "1";
        nameOfBillerEntity = "                    ";
        sendingSequence = "A";
        reserved = "                                                                               ";
    }

    public void setBillerEntityNit(String billerEntityNit) {
        this.billerEntityNit = billerEntityNit;
    }

    public void setBillerServiceIdCode(String billerServiceIdCode) {
        this.billerServiceIdCode = billerServiceIdCode;
    }

    public void setFileTransmisionDate(String fileTransmisionDate) {
        this.fileTransmisionDate = fileTransmisionDate;
    }

    public void setApplicationOrExpiration(String applicationOrExpiration) {
        this.applicationOrExpiration = applicationOrExpiration;
    }

    public void setRecordsNumber(String recordsNumber) {
        this.recordsNumber = recordsNumber;
    }

    public void setTotalAmountOfTransactions(String totalAmountOfTransactions) {
        this.totalAmountOfTransactions = totalAmountOfTransactions;
    }

    public String getRecordType() {
        return recordType;
    }

    public String getBillerEntityNit() {
        return billerEntityNit;
    }

    public String getNameOfBillerEntity() {
        return nameOfBillerEntity;
    }

    public String getBillerServiceIdCode() {
        return billerServiceIdCode;
    }

    public String getFileTransmisionDate() {
        return fileTransmisionDate;
    }

    public String getSendingSequence() {
        return sendingSequence;
    }

    public String getApplicationOrExpiration() {
        return applicationOrExpiration;
    }

    public String getRecordsNumber() {
        return recordsNumber;
    }

    public String getTotalAmountOfTransactions() {
        return totalAmountOfTransactions;
    }

    public String getReserved() {
        return reserved;
    }

    public int getLengthBillerEntityNit() {
        return lengthBillerEntityNit;
    }

    public int getLengthBillerServiceIdCode() {
        return lengthBillerServiceIdCode;
    }

    public int getLengthApplicationOfExpirationDate() {
        return lengthApplicationOfExpirationDate;
    }

    public int getLengthRecordsNumber() {
        return lengthRecordsNumber;
    }

    public int getLengthTotalAmountofTheTransactions() {
        return lengthTotalAmountofTheTransactions;
    }
}
