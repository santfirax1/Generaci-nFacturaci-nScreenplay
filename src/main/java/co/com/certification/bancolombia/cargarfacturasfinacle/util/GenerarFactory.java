package co.com.certification.bancolombia.cargarfacturasfinacle.util;

import co.com.certification.bancolombia.cargarfacturasfinacle.interactions.Generar;
import co.com.certification.bancolombia.cargarfacturasfinacle.util.enums.ExceptionCodes;
import net.serenitybdd.screenplay.Interaction;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class GenerarFactory {
    public static Interaction encabezadoDetailRecord(){
        return instrumented(Generar.class);
    }
    private GenerarFactory(){
        throw new RuntimeException(ExceptionCodes.UTILITY_CLASS.getMessage());
    }
}
