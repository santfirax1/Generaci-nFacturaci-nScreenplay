package co.com.certification.bancolombia.cargarfacturasfinacle.interactions;

import co.com.certification.bancolombia.cargarfacturasfinacle.util.LeerDataProperties;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Tasks;

import java.io.File;
import java.io.IOException;

public class Crear extends LeerDataProperties implements Interaction {
    public File getFacturaBancolombia2016() {
        return facturaBancolombia2016;
    }

    private File  facturaBancolombia2016;

    public static Crear getInstance() {
        return ourInstance;
    }

    private static  Crear ourInstance = new Crear();
    public Crear() {
        this.facturaBancolombia2016 = new File((getRutaCreacionArchivoTxt()));
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        if(!facturaBancolombia2016.exists()){
            try {
                facturaBancolombia2016.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                System.err.println(e.getMessage());
            }
        }else{
            System.err.println("ya existe un archivo en la ruta:"+ getRutaCreacionArchivoTxt());
            System.exit(1);
        }
    }

}
