package co.com.certification.bancolombia.cargarfacturasfinacle.tasks;

import co.com.certification.bancolombia.cargarfacturasfinacle.userinterface.FinacleHomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Hit;
import net.serenitybdd.screenplay.actions.Switch;
import org.openqa.selenium.Keys;

public class GoTo implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {

    }

    public static GoTo coreFirecIntSolution() {
        return Tasks.instrumented(GoTo.class);
    }
}
