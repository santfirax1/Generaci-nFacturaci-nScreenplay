package co.com.certification.bancolombia.cargarfacturasfinacle.model;

public class LogIn {
    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
