package co.com.certification.bancolombia.cargarfacturasfinacle.util;

import co.com.certification.bancolombia.cargarfacturasfinacle.model.DetailRecord;
import co.com.certification.bancolombia.cargarfacturasfinacle.util.enums.ExceptionCodes;
import com.codoid.products.fillo.Recordset;

import java.util.List;

public class AlmacenarDatos {
    private static AlmacenarDatos ourInstance = new AlmacenarDatos();
    private List<DetailRecord> detailRecordList;

    private AlmacenarDatos() {
        throw new IllegalStateException(ExceptionCodes.UTILITY_CLASS.getMessage());
    }

    public Recordset getAlmacenarDatosDeExcel() {
        return almacenarDatosDeExcel;
    }

    public void setAlmacenarDatosDeExcel(Recordset almacenarDatosDeExcel) {
        this.almacenarDatosDeExcel = almacenarDatosDeExcel;
    }

    private Recordset almacenarDatosDeExcel;
    public static AlmacenarDatos getInstance() {
        return ourInstance;
    }
}
