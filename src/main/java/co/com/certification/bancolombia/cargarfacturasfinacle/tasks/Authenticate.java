package co.com.certification.bancolombia.cargarfacturasfinacle.tasks;

import co.com.certification.bancolombia.cargarfacturasfinacle.model.LogIn;
import co.com.certification.bancolombia.cargarfacturasfinacle.userinterface.FinacleHomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.*;
import net.serenitybdd.screenplay.matchers.statematchers.*;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.openqa.selenium.support.ui.Wait;

import java.util.List;

import static co.com.certification.bancolombia.cargarfacturasfinacle.userinterface.FinacleHomePage.*;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class Authenticate implements Task {
    private List<LogIn> logIns;
    FinacleHomePage finacleHomePage = new FinacleHomePage();
    public Authenticate(List<LogIn> logIns) {
        this.logIns = logIns;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Open.browserOn(finacleHomePage));
        actor.attemptsTo(Switch.toFrame(LOG_IN_FRAME));
        actor.attemptsTo(Enter.theValue(logIns.get(0).getUsername()).into(USERNAME_FIELD));
        actor.attemptsTo(Enter.theValue(logIns.get(0).getPassword()).into(PASSWORD_FIELD));
        actor.attemptsTo(Click.on(LOG_IN_BUTTON));
        actor.attemptsTo(SelectFromOptions.byIndex(1).from(SOLUTION_SELECT));
        actor.attemptsTo(Switch.toAlert());

    }

    public static Authenticate inFinacle(List<LogIn> logInList) {
        return Tasks.instrumented(Authenticate.class,logInList);
    }
}
