package co.com.certification.bancolombia.cargarfacturasfinacle.model;

public class DetailRecord {

    public int getLengthReference1() {
        return lengthReference1;
    }

    public int getLengthReference2() {
        return lengthReference2;
    }

    public int getLengthPayersNit() {
        return lengthPayersNit;
    }

    public int getLengthTransactionAmount() {
        return lengthTransactionAmount;
    }

    private int lengthReference1;
    private int lengthReference2;
    private int lengthPayersNit;
    private int lengthTransactionAmount;
    private String recordType;
    private String payersNit;

    public String getRecordType() {
        return recordType;
    }

    public String getPayersName() {
        return payersName;
    }

    public String getPayersAccountBank() {
        return payersAccountBank;
    }

    public String getAccountNumberToBeDebited() {
        return accountNumberToBeDebited;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public String getBilledPeriods() {
        return billedPeriods;
    }

    public String getCycle() {
        return cycle;
    }

    public String getReserved() {
        return reserved;
    }

    public String getValidationNit() {
        return validationNit;
    }

    private String payersName;
    private String payersAccountBank;
    private String accountNumberToBeDebited;
    private String transactionType;
    private String transactionAmount;
    private String referenceOne;
    private String referenceTwo;
    private String expirationDateOrApplicationDate;
    private String billedPeriods;
    private String cycle;
    private String reserved;
    private String validationNit;
    private String encabezadoTotal;

    public String getEncabezadoTotal() {
        return encabezadoTotal;
    }

    public void setEncabezadoTotal(String encabezadoTotal) {
        this.encabezadoTotal = encabezadoTotal;
    }

    public DetailRecord() {
        lengthReference1 = 30;
        lengthReference2 = 30;
        lengthPayersNit = 13;
        lengthTransactionAmount = 17;
        recordType = "6";
        payersName = "                    ";
        payersAccountBank = "000000000";
        accountNumberToBeDebited = "                 ";
        transactionType = "  ";
        reserved = "                 ";
        validationNit = "N";
        billedPeriods = "  ";
        cycle = "   ";

    }

    public void setTransactionAmount(String transactionAmount) {
        this.transactionAmount = transactionAmount;
    }


    public String getPayersNit() {
        return payersNit;
    }

    public String getTransactionAmount() {
        return transactionAmount;
    }

    public String getReferenceOne() {
        return referenceOne;
    }

    public String getReferenceTwo() {
        return referenceTwo;
    }

    public String getExpirationDateOrApplicationDate() {
        return expirationDateOrApplicationDate;
    }

    public void setPayersNit(String payersNit) {
        this.payersNit = payersNit;
    }

    public void setReferenceOne(String referenceOne) {
        this.referenceOne = referenceOne;
    }

    public void setReferenceTwo(String referenceTwo) {
        this.referenceTwo = referenceTwo;
    }

    public void setExpirationDateOrApplicationDate(String expirationDateOrApplicationDate) {
        this.expirationDateOrApplicationDate = expirationDateOrApplicationDate;
    }

}
