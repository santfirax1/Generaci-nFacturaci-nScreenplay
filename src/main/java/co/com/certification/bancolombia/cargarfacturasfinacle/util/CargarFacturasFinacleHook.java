package co.com.certification.bancolombia.cargarfacturasfinacle.util;

import cucumber.api.java.Before;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

public class CargarFacturasFinacleHook {
    @Before
    public void setup(){
        OnStage.setTheStage(new OnlineCast());
    }
}
