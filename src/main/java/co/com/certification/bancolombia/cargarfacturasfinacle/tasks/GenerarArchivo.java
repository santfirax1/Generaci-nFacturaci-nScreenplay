package co.com.certification.bancolombia.cargarfacturasfinacle.tasks;

import co.com.certification.bancolombia.cargarfacturasfinacle.interactions.LeerDatos;
import co.com.certification.bancolombia.cargarfacturasfinacle.util.GenerarFactory;
import co.com.certification.bancolombia.cargarfacturasfinacle.util.QueryManager;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class GenerarArchivo implements Task {
    String query=QueryManager.QUERY_EXCEL.getString("SQL.SELECT.consultarDatos");
    @Override
    public <T extends Actor> void performAs(T actor) {

        theActorInTheSpotlight().attemptsTo(LeerDatos.deArchivoExcel(query));
        theActorInTheSpotlight().attemptsTo(GenerarFactory.encabezadoDetailRecord());
        theActorInTheSpotlight().attemptsTo(LeerDatos.deArchivoExcel("select * from Datos2"));


    }

    public static GenerarArchivo bancolombia2016() {
        return Tasks.instrumented(GenerarArchivo.class);
    }
}
