package co.com.certification.bancolombia.cargarfacturasfinacle.util;

import co.com.certification.bancolombia.cargarfacturasfinacle.interactions.Crear;
import co.com.certification.bancolombia.cargarfacturasfinacle.interactions.Escribir;
import net.serenitybdd.screenplay.Interaction;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class ArchivoFactory {
    public static Interaction crearArchivoTxt(){
        return instrumented(Crear.class);
    }
    public static Interaction escribirEnElArchivoTxt(String data){
        return instrumented(Escribir.class,data);
    }

}
