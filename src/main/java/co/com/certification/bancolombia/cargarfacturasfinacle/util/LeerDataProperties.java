package co.com.certification.bancolombia.cargarfacturasfinacle.util;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class LeerDataProperties {
    private String rutaCreacionArchivoTxt;
    private String rutaArchivoExcel;
    protected static final Properties archivoPropiedades = new Properties();
    public LeerDataProperties()  {
        try {
            archivoPropiedades.load(new FileReader("C:\\Users\\jmolanop\\Downloads\\GeneracionFacturas\\src\\main\\resources\\data.properties"));
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("no se pudo el el archivo de propiedades");
        }
        rutaCreacionArchivoTxt = archivoPropiedades.getProperty("rutadearchivotxt");
        rutaArchivoExcel=archivoPropiedades.getProperty("rutadearchivoexcel");

    }

    public String getRutaCreacionArchivoTxt() {
        return rutaCreacionArchivoTxt;
    }

    public String getRutaArchivoExcel() {
        return rutaArchivoExcel;
    }
}
