package co.com.certification.bancolombia.cargarfacturasfinacle.userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;

@DefaultUrl("https://wsfinaclerecfit.bancolombia.corp:9443/fininfra/ui/SSOLogin.jsp?LANG=es_ES")
public class FinacleHomePage extends PageObject {
    public static final String LOG_IN_FRAME = "loginFrame";
    public static final Target USERNAME_FIELD = Target.the("username field").located(By.id("usertxt"));
    public static final Target PASSWORD_FIELD = Target.the("password field").located(By.id("passtxt"));
    public static final Target LOG_IN_BUTTON = Target.the("log in button").located(By.id("Submit"));
    public static final Target SOLUTION_SELECT = Target.the("solution select options").located(By.id("appSelect"));
    public static final String FRAME_MENU = "menutree";
    public static final Target MENU_TABLE = Target.the("menu table").located(By.id("dijit_layout_BorderContainer_0"));
}
