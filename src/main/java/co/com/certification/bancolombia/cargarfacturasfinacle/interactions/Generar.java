package co.com.certification.bancolombia.cargarfacturasfinacle.interactions;

import co.com.certification.bancolombia.cargarfacturasfinacle.model.DetailRecord;
import co.com.certification.bancolombia.cargarfacturasfinacle.util.AlmacenarDatos;
import co.com.certification.bancolombia.cargarfacturasfinacle.util.ManejarCadenas;
import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Tasks;

import java.util.ArrayList;
import java.util.List;

public class Generar extends ManejarCadenas implements Interaction {
    private List<DetailRecord> detailRecordList = new ArrayList<>();
    private Recordset dataTraidaDeExcel = AlmacenarDatos.getInstance().getAlmacenarDatosDeExcel();

    @Override
    public <T extends Actor> void performAs(T actor) {
        try {
            while (dataTraidaDeExcel.next()) {
                if (!dataTraidaDeExcel.getField("Referencia1").equals("")) {
                    DetailRecord detailRecord = new DetailRecord();
                    detailRecord.setReferenceOne(rellenarCadenaConEspaciosEnBlanco(dataTraidaDeExcel.getField("Referencia1"), detailRecord.getLengthReference1()));
                    detailRecord.setReferenceTwo(rellenarCadenaConEspaciosEnBlanco(dataTraidaDeExcel.getField("Referencia2"), detailRecord.getLengthReference2()));
                    detailRecord.setPayersNit(rellenarCadenaConEspaciosEnBlanco(dataTraidaDeExcel.getField("Referencia3"), detailRecord.getLengthPayersNit()).replace(' ', '0'));
                    detailRecord.setTransactionAmount(rellenarCadenaConEspaciosEnBlanco(dataTraidaDeExcel.getField("Valor").replace(",", ""), detailRecord.getLengthTransactionAmount()).replace(' ', '0'));
                    if(dataTraidaDeExcel.getField("FechaDeVencimiento").isEmpty()){
                        detailRecord.setExpirationDateOrApplicationDate(calculaFechaDeVencimientoOFechaDeAplicacionParaLasFacturas(true));
                    }else{
                        detailRecord.setExpirationDateOrApplicationDate(dataTraidaDeExcel.getField("FechaDeVencimiento").replace("/",""));
                    }

                    detailRecordList.add(detailRecord);


                }
          }
            for (DetailRecord detailRecord : detailRecordList) {
                detailRecord.setEncabezadoTotal(detailRecord.getRecordType() + detailRecord.getPayersNit() + detailRecord.getPayersName() + detailRecord.getPayersAccountBank() + detailRecord.getAccountNumberToBeDebited() + detailRecord.getTransactionType() + detailRecord.getTransactionAmount() + detailRecord.getValidationNit() + detailRecord.getReferenceOne() + detailRecord.getReferenceTwo() + detailRecord.getExpirationDateOrApplicationDate() + detailRecord.getBilledPeriods() + detailRecord.getCycle() + detailRecord.getReserved());
                System.out.println(detailRecord.getEncabezadoTotal());

            }

        } catch (FilloException e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
    }


}
