package co.com.certification.bancolombia.cargarfacturasfinacle.util;

import co.com.certification.bancolombia.cargarfacturasfinacle.model.DetailRecord;

import java.util.Calendar;

public class ManejarCadenas {
    protected String rellenarCadenaConEspaciosEnBlanco(String cadenaParaRellenar, int sizeCadenaTotal) {
        int size = sizeCadenaTotal - cadenaParaRellenar.length();
        if (size > 0) {
            String formato = "%-" + size + "s";
            String rellenar = String.format(formato, " ");
            cadenaParaRellenar = rellenar + cadenaParaRellenar;
        }
        return cadenaParaRellenar;
    }
    protected String calculaFechaDeVencimientoOFechaDeAplicacionParaLasFacturas(Boolean obtenerFechaVencimientoOFechaAplicacion) {
        String year, month, day = "01", fechaDeVencimiento;
        int i = 0;
        Calendar calendario = Calendar.getInstance();
        if (i == 0) {
            if (calendario.get((Calendar.DATE)) == 30) {
                day = "01";
            } else if (!obtenerFechaVencimientoOFechaAplicacion) {
                day = Integer.toString(calendario.get((Calendar.DATE)));
            } else {
                day = Integer.toString(calendario.get((Calendar.DATE)) + 1);
            }
            i++;
        }

        month = Integer.toString(calendario.get((Calendar.MONTH)) + 1);
        if (obtenerFechaVencimientoOFechaAplicacion) {
            year = Integer.toString(calendario.get((Calendar.YEAR)) + 1);

        } else {
            year = Integer.toString(calendario.get(Calendar.YEAR));
        }
        fechaDeVencimiento = year + month + day;
        if (fechaDeVencimiento.length() < 8) {
            fechaDeVencimiento = fechaDeVencimiento.concat("0");
        }
        return fechaDeVencimiento;
    }

}
