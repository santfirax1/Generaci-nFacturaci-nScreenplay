package co.com.certification.bancolombia.cargarfacturasfinacle.util.enums;

public enum ExceptionCodes {
    UTILITY_CLASS(1,"Clase de utilidades"),
    FACTORY_CLASS(2,"Clase Factory");
    private String message;
    private int id;

    ExceptionCodes(int id, String message) {
        this.message = message;
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public int getId() {
        return id;
    }
}
