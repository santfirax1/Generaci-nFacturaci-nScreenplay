package co.com.certification.bancolombia.cargarfacturasfinacle.interactions;

import co.com.certification.bancolombia.cargarfacturasfinacle.util.LeerDataProperties;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;

import java.io.FileWriter;
import java.io.IOException;

public class Escribir extends LeerDataProperties implements Interaction {
    private String encabezadoTotal;
    private Crear archivoBancolombia2016 = Crear.getInstance();
    private Escribir(String encabezadoTotal) {
        this.encabezadoTotal = encabezadoTotal;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        try {
            FileWriter escribirInformacion = new FileWriter(archivoBancolombia2016.getFacturaBancolombia2016(),true);
            escribirInformacion.write(encabezadoTotal+"\n");
            escribirInformacion.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println(e.getMessage());
        }
    }
}
