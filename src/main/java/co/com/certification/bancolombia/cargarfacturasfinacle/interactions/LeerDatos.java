package co.com.certification.bancolombia.cargarfacturasfinacle.interactions;

import co.com.certification.bancolombia.cargarfacturasfinacle.util.AlmacenarDatos;
import co.com.certification.bancolombia.cargarfacturasfinacle.util.LeerDataProperties;
import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;

public class LeerDatos extends LeerDataProperties implements Interaction {
    private Connection objConnection;
    private AlmacenarDatos dataTraidaDeExcel = AlmacenarDatos.getInstance();
    private String query;

    private LeerDatos(String query) {
        this.query = query;
    }

    public static LeerDatos deArchivoExcel(String query) {
        return Instrumented.instanceOf(LeerDatos.class).withProperties(query);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        try {
            Fillo dataExcel = new Fillo();
            objConnection = dataExcel.getConnection(getRutaArchivoExcel());
            dataTraidaDeExcel.setAlmacenarDatosDeExcel(objConnection.executeQuery(query));
        } catch (FilloException e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
    }
}
