package co.com.certification.bancolombia.cargarfacturasfinacle.util;

import co.com.certification.bancolombia.cargarfacturasfinacle.util.enums.ExceptionCodes;

import java.util.ResourceBundle;

public class QueryManager {
    private QueryManager() {
        throw new IllegalStateException(ExceptionCodes.UTILITY_CLASS.getMessage());
    }

    public static final ResourceBundle QUERY_EXCEL = ResourceBundle.getBundle("consultaExcel");
}
